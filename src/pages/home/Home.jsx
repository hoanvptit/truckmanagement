import Header from '../../components/header/Header'
import LeftSide from '../../components/body/left_side/LeftSide'
import ListTruck from '../../components/body/list_truck/ListTruck';
import './home.css'

export default function Home() {
    return (
        <div className="App">
            <Header />
            <div className='wrapper'>
                <LeftSide idx='0'/>
                <div className='main'>
                    <ListTruck />
                </div>
            </div>
        </div>
    );
}