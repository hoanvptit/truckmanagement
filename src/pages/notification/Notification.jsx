import React from 'react'
import Header from '../../components/header/Header'
import LeftSide from '../../components/body/left_side/LeftSide'
import NotifyCard from '../../components/notify/NotifyCard'
import { notifyService } from '../../services/notify'
import no_room from '../../assets/no_room.png'
import {onHasNoti1} from '../../services/Socket'
import NotiPopup from '../../components/notify/NotiPopup'


export default function Notification(){

    const [listNotify, setListNotify] = React.useState([]);
    const [isLoading, setIsLoading] = React.useState(true);
    const [showNoti, setShowNoti] = React.useState(false);
    const [noti, setNoti] = React.useState(null);

    const fetchData = async () => {
        const {data} = await notifyService.getNotify();
        // console.log(data);
        data.sort(function(a, b){return - new Date(a.time).getTime() + new Date(b.time).getTime()})
        // console.log(data)
        setListNotify(data);
    }

    React.useEffect(async()=>{
        if(isLoading){
            setIsLoading(false)
            fetchData();
            onHasNoti1(fetchData);
        }
    }, [listNotify])

    return (
        <div className="App">
            {showNoti && <NotiPopup noti={noti} hide={() => setShowNoti(false)}/>}
            <Header />
            <div className='wrapper'>
                <LeftSide idx='2'/>
                <div className='main'>
                    <div className='nt_cnt'>
                        <h2 className="crtitle">Thông báo</h2>
                        {
                            listNotify.length===0 ?
                            <>
                                <div className='noti_content'>
                                    <img src={no_room} />
                                    <h3>Không có thông báo nào</h3>
                                </div>
                            </>
                            :<div className='notibody_cnt'>
                                {
                                    listNotify.map((val, index) => <NotifyCard 
                                                                        noti={val} 
                                                                        key={index}
                                                                        handleClick={() => {
                                                                            setNoti(val);
                                                                            setShowNoti(true);
                                                                        }}
                                                                    />)
                                }
                            </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}