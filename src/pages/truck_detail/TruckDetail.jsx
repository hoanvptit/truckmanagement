import React from 'react'
import { useParams } from 'react-router-dom';
import LeftSide from '../../components/body/left_side/LeftSide'
import Header from '../../components/header/Header'
import {routeService} from '../../services/route'
import {truckService} from '../../services/truck'
import Map from '../../components/map/Map';
import moment from 'moment';
import {onImage} from '../../services/Socket'


function TruckDetail() {
  const { id } = useParams();
  const [routeOfTruck, setRouteOfTruck] = React.useState([]);
  const [truckInfo, setTruckInfor] = React.useState({})
  const [center, setCenter] = React.useState( {lat: 20.929161, lng: 105.669589})
  // const [selectDate, setSelectDate] = React.useState(new Date());
  const [isConnectedSocket, setIsConnectedSocket] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);

  const fetchRoute = async (date) => {
    const {data} = await routeService.getRouteOfTruck(id, date);
    setRouteOfTruck(data);
    if(data.length > 0){
      data.sort((a, b) => a.lat < b.lat);
      const latmid  = data[data.length/2].lat
      data.sort((a, b) => a.lng < b.lng);
      const lngmid  = data[data.length/2].lng
      setCenter({lat: latmid, lng: lngmid})
      // console.table('tab', center);
    }
  }

  const fetchTruck = async () => {
    const {data} = await truckService.getById(id);
    setTruckInfor(data)
    if(!isConnectedSocket){
      setIsConnectedSocket(true);
      console.log(data.license_plate)
      onImage(data?.license_plate, (time, data) => {
        document.getElementById('img123').src = data;
        document.getElementById('time123').innerHTML = 'Thời gian: '+moment(new Date(time*1000)).format('HH:mm:ss  DD/MM/yyyy');
      })
    }
  }

  React.useEffect(async () => {
    if(!isLoading){
      setIsLoading(true)
      fetchTruck()
      fetchRoute(new Date())
      
    }
  }, [truckInfo, routeOfTruck])

  return (
    <div className="App">
        <Header />
        <div className='wrapper'>
          <LeftSide />
          <div className='main'>
            <div className="td_cnt">
              <div className="td_wp">
                <div className="td_truck_infor">
                  <div className='tti_left'>
                    <div className="row">
                      <h3>Biển số : </h3>
                      <p className='txt'>{truckInfo?.license_plate}</p>
                    </div>
                    <div className="row">
                      <h3>
                      <i className="fa-solid fa-apartment"></i>
                        Khu vực hoạt động : 
                      </h3>
                      <p className='txt'>{truckInfo?.area}</p>
                    </div>
                  </div>
                  <div className='tti_right'>
                    <div className="row">
                      <h3>Khu vực tập kết : </h3>
                      <p className='txt'>{truckInfo?.dump_address}</p>
                    </div>
                    <div className="row">
                      <h3>Khu vực đổ rác : </h3>
                      <p className='txt'>{truckInfo?.gathering_address}</p>
                    </div>
                  </div>
                  
                </div>
                <div className="td_route">
                  <div className="table_cnt">
                    <div className='tb_title_row'>
                      <h3>
                        <i class="fa-solid fa-table-cells-large"></i>
                        Lộ trình
                      </h3>
                      <h3>
                        {/* Ngày: */}
                        <input id='datex' type='date' defaultValue={new Date().toISOString().split('T')[0]} 
                           onChange={(e) => {
                            fetchRoute(moment(e.target.value))
                        }}/>
                      </h3>
                    </div>
                    <div className="tb_wp">
                      <table className="tb_route">
                          <tr className='tb_row hd'>
                            <td>STT</td>  
                            <td>Thời gian</td>  
                            <td>Địa chỉ</td>  
                            <td>Kinh độ</td>  
                            <td>Vĩ độ</td> 
                            <td>Ghi chú</td> 
                          </tr>  
                          {
                            routeOfTruck.map((val, index) => {
                              return (
                                      <tr className='tb_row'>
                                        <td>{index+1}</td>  
                                        <td>{moment(val?.time).format('HH:mm:ss')}</td>  
                                        <td>{val?.address}</td>  
                                        <td>{val?.lat}</td>  
                                        <td>{val?.lng}</td>  
                                        <td>Đúng KV</td>
                                      </tr> 
                              )
                            })
                          }
                      </table>
                    </div>  
                  </div>
                  <div className="stream_cnt">  
                    <h3 className='str_tt'>
                      <i class="fa-solid fa-clock-rotate-left"></i>
                      <p id='time123'>{`Thời gian: 00:00 00-00-0000`}</p>
                    </h3>
                    <img id = 'img123' src='https://booktrip.me/themes/default/assets/img/loading.gif' alt='' />
                  </div>
                </div>
                <div className="map_cnt">
                  <h3>
                  <i class="fa-solid fa-location-dot"></i>
                    Bản đồ 
                  </h3>
                  <Map center={center} listPoint={routeOfTruck} height={"500px"} isRoute={true} />
                </div>
              </div>  
            </div>
          </div>
        </div>
    </div>
    
  )
}

export default TruckDetail