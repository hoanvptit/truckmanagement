import { useEffect, useState } from "react";
import axios from 'axios';
import CardTrucK from '../../components/body/card_truck/CardTruck';
import { Redirect, useLocation, useParams } from "react-router-dom";
import Header from "../../components/header/Header";
import LeftSide from "../../components/body/left_side/LeftSide";
import no_room from '../../assets/no_room.png'
// import './search.css'
export default function ResultSearch() {
    const _param = useParams().params
    const [isLoading, setLoading] = useState(true);
    const [haveData, setHaveData] = useState(false);
    const [listTruck1, setListTruck1] = useState([]);
    const [listTruck2, setListTruck2] = useState([]);

    let inTruck = [];
    let urlSearch = null;
    urlSearch = 'http://localhost:5000/api/trucks/search'

    
    useEffect(() => {
        // console.log("in eff: ");
        fetchSearchTruck(_param);
        fetchSearchTruckArea()
    }, [_param])

    const fetchSearchTruck = (_license_plate) => {
        setHaveData(false);
        axios.get(urlSearch.concat(`/${_license_plate}`)).then((res) => {
            // console.log("res1: ", res)
            const result = res.data;
            if (result.length > 0) {
                setListTruck1(result);
                setHaveData(true);
            }
            setLoading(false);
        }).catch((err) => console.log(err))

    }
    const fetchSearchTruckArea = () =>{
        // setHaveData(false);
        // console.log("param: ", _param)
        axios.post(urlSearch,{
            "license_plate":"",
            "area": {_param}
        }).then((res) => {
            console.log("res2: ", res)
            const result = res.data;
            if (result.length > 0) {
                setListTruck2(result);
                setHaveData(true);
            }
            setLoading(false);
        }).catch((err) => console.log(err))
    }
    const arrayUnique = (array) => {
        var a = array.concat();
        for(var i=0; i<a.length; ++i) {
            for(var j=i+1; j<a.length; ++j) {
                if(a[i]._id === a[j]._id)
                    a.splice(j--, 1);
            }
        }
    
        return a;
    }
    const TruckSearchLine = () => {
        let listTruck = arrayUnique(listTruck1.concat(listTruck2))
        return listTruck.map((truck, index) => {
            return <CardTrucK key={index} truck={truck} />
        })
    }
    var body = null;
    if (isLoading) {
        body = <div className="container_center"><h3>Loading.... </h3></div>
    } else if (!haveData) {
        body =
            <div className="container_center">
                <div className="CardTrucK_state">
                    <h3>Kết quả tìm kiếm:</h3>
                    <div className="inform_no_room">
                        <img src={no_room} className="img_no_room" /><br />
                        <span>Không tìm thấy xe, vui lòng kiểm tra lại biển số xe!</span>
                    </div>
                </div>

                <div className="CardTrucK_list">
                </div>
                <div className="space"></div>
            </div>

    } else {
        body =
            <div className="container_center">
                <div className="CardTrucK_state">
                    <h3>Kết quả tìm kiếm:</h3>
                    {/* <span className="num-CardTrucK">{listTruck.length}</span> */}
                </div>
                <div className="CardTrucK_list">
                    <TruckSearchLine />
                </div>
                <div className="space"></div>
            </div>
    }

    return (
        <div className="App">
            <Header />
            <div className='wrapper'>
                <LeftSide />
                <div className='main'>
                    {body}
                </div>
            </div>
        </div>
    )
}