import report_icon from '../../assets/notify.png'
import {MdLocationCity} from 'react-icons/md'
import {FaDumpsterFire, FaDumpster } from 'react-icons/fa'
import { Button } from 'react-bootstrap'
import Header from '../../components/header/Header'
import LeftSide from '../../components/body/left_side/LeftSide'
import { useState } from 'react';
import axios from 'axios';
import PopupConfirm from '../../popup/popup_confirm'
// import './tructcreation.css';

const initTruck = {
    license_plate: '',
    area: '',
    id_gis: '',
    dump_address: '',
    gathering_address: ''
}
function TruckCreation({ match }) {
    const url = 'http://localhost:5000/api/trucks'
    const [truckInfo, setTruckInfo] = useState(initTruck)
    const [modalShow, setModalShow] = useState(false)
    const [message, setMessage] = useState("")
    const [btn_text, setBtnText] = useState("")
    // const [address, setAddress] = useState("")
    const OnChangeBsx = (e) => {
        let license_plate = e.target.value
        setTruckInfo((prev) => {
            return {
                ...prev,
                license_plate: license_plate
            }
        })
    }
    const OnChangeId = (e) => {
        let id = e.target.value
        setTruckInfo((prev) => {
            return {
                ...prev,
                id_gis: id
            }
        })
    }
    const onChangeKV = (e) => {
        let kv = e.target.value;
        if (kv !== 'all') setTruckInfo((prev) => {
            return {
                ...prev,
                area: kv
            }
        })
    }
    const onChangeDtk = (e) => {
        let kv = e.target.value;
        if (kv !== 'all') setTruckInfo((prev) => {
            return {
                ...prev,
                gathering_address: kv
            }
        })
    }
    const onChangeDdr = (e) => {
        let kv = e.target.value;
        if (kv !== 'all') setTruckInfo((prev) => {
            return {
                ...prev,
                dump_address: kv
            }
        })
    }
    const handleCancel = () => {
        setTruckInfo(initTruck)
    }
    const handleCreate = () => {
        let arr = Object.values(truckInfo)
        let check = true
        arr.map(element => {
            if (element.trim() === '') {
                check = false
                return
            }
        })
        if (check) {
            doPost(url)
        }else{
            setMessage('Xảy ra lỗi, vui lòng kiểm tra lại')
            setBtnText('Xác nhận')
            setModalShow(true);
        }
    }
    const doPost = (url) => {
        // console.log('cre info: ', truckInfo)
        axios.post(url, {
            ...truckInfo
        })
            .then(response => {
                console.log(response);
                if (response.status == 200) {
                    setMessage('Thêm xe thành công')

                } else {
                    setMessage('Xảy ra lỗi, vui lòng kiểm tra lại')
                }
                setModalShow(true);
                setBtnText('Xác nhận')

            })
            .catch(error => {
                console.log(error)
                setMessage('Xảy ra lỗi, vui lòng kiểm tra lại')
                setBtnText('Xác nhận')
                setModalShow(true);
            });
    }
    var body =
        <div className="tch_main  apear_animation">
            {modalShow && <PopupConfirm
                title="Thêm xe"
                handleConfirm={() => setModalShow(false)}
                textBtn={btn_text}
                message={message}
            />}
            <div className="create-room-body">
                <h2 className="crtitle">Thêm xe</h2>
                <div className="content">
                    <div className="content-row">
                        <div className="ls-card time-select">
                            {/* <img src={report_icon} className="ls-icon" alt="time img" /> */}
                            <img src="https://img.icons8.com/external-smashingstocks-glyph-smashing-stocks/66/000000/external-license-plate-car-repair-smashingstocks-glyph-smashing-stocks.png"
                            className="ls-icon" alt="time img"/>
                            <p className="ls-title">Biển số xe </p>
                        </div>
                        <div className="subject_name">
                            <input
                                className='input_subject bsx'
                                type="text"
                                name="bsx"
                                placeholder='Biển số xe'
                                value={truckInfo.license_plate}
                                onChange={OnChangeBsx}
                            />
                        </div>
                    </div>
                    <div className="content-row">
                        <div className="ls-card time-select">
                            <img src={report_icon} className="ls-icon" alt="time img" />
                            {/* <FontAwesomeIcon icon="fa-solid fa-sensor-on" /> */}
                            <p className="ls-title">ID của cảm biến </p>
                        </div>
                        <div className="subject_name">
                            <input
                                className='input_subject bsx'
                                type="text"
                                name="bsx"
                                placeholder='ID của cảm biến'
                                value={truckInfo.id_gis}
                                onChange={OnChangeId}
                            />
                        </div>
                    </div>
                    <div className="content-row">
                        <div className="ls-card time-select">
                            {/* <img src={report_icon} className="ls-icon" alt="time img" /> */}
                            <MdLocationCity className="ls-icon" />
                            <p className="ls-title">Khu vực hoạt động </p>
                        </div>
                        <div className="subject_name">
                            <select
                                id="select"
                                name="files"
                                className="input_subject"
                                onChange={onChangeKV}
                            >
                                <option value="all" className="input_subject">Chọn khu vực hoạt động</option>
                                <option value="Thanh Xuân" className="input_subject">Thanh Xuân</option>
                                <option value="Hà Đông" className="input_subject">Hà Đông</option>
                                <option value="Cầu Giấy" className="input_subject">Cầu Giấy</option>
                                <option value="Hoàn Kiếm" className="input_subject">Hoàn Kiếm</option>
                                <option value="Đống Đa" className="input_subject">Đống Đa</option>
                            </select>
                        </div>
                    </div>
                    <div className="content-row">
                        <div className="ls-card time-select">
                            {/* <img src={report_icon} className="ls-icon" alt="time img" /> */}
                            <FaDumpster className="ls-icon"/>
                            <p className="ls-title">Điểm tập kết </p>
                        </div>
                        <div className="subject_name">
                            <select
                                id="select"
                                name="files"
                                className="input_subject"
                                onChange={onChangeDtk}
                            >
                                <option value="all" className="input_subject">Chọn Điểm tập kết</option>
                                <option value="Thanh Xuân" className="input_subject">Thanh Xuân</option>
                                <option value="Hà Đông" className="input_subject">Hà Đông</option>
                                <option value="Cầu Giấy" className="input_subject">Cầu Giấy</option>
                                <option value="Hoàn Kiếm" className="input_subject">Hoàn Kiếm</option>
                                <option value="Đống Đa" className="input_subject">Đống Đa</option>
                            </select>
                        </div>
                    </div>
                    <div className="content-row">
                        <div className="ls-card time-select">
                            {/* <img src={report_icon} className="ls-icon" alt="time img" /> */}
                            <FaDumpsterFire className="ls-icon"/>
                            <p className="ls-title">Điểm đổ rác </p>
                        </div>
                        <div className="subject_name">
                            <select
                                id="select"
                                name="files"
                                className="input_subject"
                                onChange={onChangeDdr}
                            >
                                <option value="all" className="input_subject">Chọn điểm đổ rác</option>
                                <option value="Thanh Xuân" className="input_subject">Thanh Xuân</option>
                                <option value="Hà Đông" className="input_subject">Hà Đông</option>
                                <option value="Cầu Giấy" className="input_subject">Cầu Giấy</option>
                                <option value="Hoàn Kiếm" className="input_subject">Hoàn Kiếm</option>
                                <option value="Đống Đa" className="input_subject">Đống Đa</option>
                            </select>
                        </div>
                    </div>
                    <div className="btn-group_cr">
                        <Button className="btn-cancel btn_cancel"
                            onClick={(e) => handleCancel(e)}
                        >Huỷ bỏ</Button>
                        <Button className="btn-create btn_cr"
                            onClick={(e) => handleCreate(e)}
                        >Thêm xe</Button>
                    </div>
                </div>
            </div>
            <div className="space"></div>
        </div>
    return (
        <div className="App">
            <Header />
            <div className='wrapper'>
                <LeftSide idx='1'/>
                <div className='main'>
                    <div className='container_center'>
                        {body}
                    </div>
                </div>
            </div>
        </div>

    )
}
export default TruckCreation