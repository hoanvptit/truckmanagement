import Header from '../../components/header/Header'
import LeftSide from '../../components/body/left_side/LeftSide'
import ListTruck from '../../components/body/list_truck/ListTruck';
import '../home/home.css'

export default function Instruction() {
    return (
        <div className="App">
            <Header />
            <div className='wrapper'>
                <LeftSide idx='0'/>
                <div className='main'>
                   <h3 style={{textAlign:'center'}}>Hướng dẫn</h3>
                </div>
            </div>
        </div>
    );
}