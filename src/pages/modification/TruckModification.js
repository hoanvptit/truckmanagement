
import report_icon from '../../assets/notify.png'
import { Button } from 'react-bootstrap'
import { MdLocationCity } from 'react-icons/md'
import { FaDumpsterFire, FaDumpster } from 'react-icons/fa'
import Header from '../../components/header/Header'
import LeftSide from '../../components/body/left_side/LeftSide'
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import PopupConfirm from '../../popup/popup_confirm'
import axios from 'axios';
// import './truckModify.css';

const initTruck = {
    license_plate: '',
    area: '',
    id_gis: '',
    dump_address: '',
    gathering_address: ''
}
export default function TruckModification() {
    let truck_id = useParams().truck_id
    const url = 'http://localhost:5000/api/trucks'
    const [truckInfo, setTruckInfo] = useState(initTruck)
    const [oldTruckInfo, setOldTruckInfo] = useState(initTruck)

    const [modalShow, setModalShow] = useState(false)
    const [message, setMessage] = useState("")
    const [btn_text, setBtnText] = useState("")
    useEffect(() => {
        doFetchTruck(`${url}/${truck_id}`)
    }, [truck_id])

    const doFetchTruck = (url) => {
        axios.get(url)
            .then(res => {
                if (res.status == 200) {
                    let data = res.data
                    setTruckInfo((prev) => {
                        return {
                            ...prev,
                            license_plate: data.license_plate,
                            area: data.area,
                            id_gis: data.id_gis,
                            dump_address: data.dump_address,
                            gathering_address: data.gathering_address
                        }
                    })
                    setOldTruckInfo((prev) => {
                        return {
                            ...prev,
                            license_plate: data.license_plate,
                            area: data.area,
                            id_gis: data.id_gis,
                            dump_address: data.dump_address,
                            gathering_address: data.gathering_address
                        }
                    })
                }
            })
            .catch(err => {
                console.log('err: ', err)
            })
    }
    const OnChangeBsx = (e) => {
        let license_plate = e.target.value
        setTruckInfo((prev) => {
            return {
                ...prev,
                license_plate: license_plate
            }
        })
    }
    const OnChangeId = (e) => {
        let id = e.target.value
        setTruckInfo((prev) => {
            return {
                ...prev,
                id_gis: id
            }
        })
    }
    const onChangeKV = (e) => {
        let kv = e.target.value;
        if (kv !== 'all') setTruckInfo((prev) => {
            return {
                ...prev,
                area: kv
            }
        })
    }
    const onChangeDtk = (e) => {
        let kv = e.target.value;
        if (kv !== 'all') setTruckInfo((prev) => {
            return {
                ...prev,
                gathering_address: kv
            }
        })
    }
    const onChangeDdr = (e) => {
        let kv = e.target.value;
        if (kv !== 'all') setTruckInfo((prev) => {
            return {
                ...prev,
                dump_address: kv
            }
        })
    }
    const handleCancel = () => {
        setTruckInfo(oldTruckInfo)
    }
    const handleSave = () => {
        // console.log('info: ', truckInfo)
        let arr = Object.values(truckInfo)
        let check = true
        arr.map(element => {
            if (element.trim() === '') {
                check = false
                return
            }
        })
        if (check) {
            doUpdate(`${url}/${truck_id}`)
        } else {
            setMessage('Xảy ra lỗi, vui lòng kiểm tra lại')
            setBtnText('Xác nhận')
            setModalShow(true);
        }
    }
    const doUpdate = (url) => {
        axios.put(url, {
            ...truckInfo
        })
            .then(response => {
                console.log(response);
                if (response.status == 200) {
                    setMessage('Sửa thông tin xe thành công')

                } else {
                    setMessage('Xảy ra lỗi, vui lòng kiểm tra lại')
                }
                setBtnText('Xác nhận')
                setModalShow(true);
            })
            .catch(error => {
                console.log(error);
                setMessage('Xảy ra lỗi, vui lòng kiểm tra lại')
                setBtnText('Xác nhận')
                setModalShow(true);
            });
    }
    var body =
        <div className="tch_main  apear_animation">
            {modalShow && <PopupConfirm
                title="Sửa thông tin xe"
                handleConfirm={() => setModalShow(false)}
                textBtn={btn_text}
                message={message}
            />}
            <div className="create-room-body">
                <h2 className="crtitle">Sửa thông tin xe</h2>
                <div className="content">
                    <div className="content-row">
                        <div className="ls-card time-select">
                            {/* <img src={report_icon} className="ls-icon" alt="time img" /> */}
                            <img src="https://img.icons8.com/external-smashingstocks-glyph-smashing-stocks/66/000000/external-license-plate-car-repair-smashingstocks-glyph-smashing-stocks.png"
                                className="ls-icon" alt="time img" />

                            <p className="ls-title">Biển số xe </p>
                        </div>
                        <div className="subject_name">
                            <input
                                className='input_subject bsx'
                                type="text"
                                name="bsx"
                                placeholder='Biển số xe'
                                value={truckInfo.license_plate}
                                onChange={OnChangeBsx}
                            />
                        </div>
                    </div>
                    <div className="content-row">
                        <div className="ls-card time-select">
                            <img src={report_icon} className="ls-icon" alt="time img" />
                            <p className="ls-title">ID của cảm biến </p>
                        </div>
                        <div className="subject_name">
                            <input
                                className='input_subject bsx'
                                type="text"
                                name="bsx"
                                placeholder='ID của cảm biến'
                                value={truckInfo.id_gis}
                                onChange={OnChangeId}
                            />
                        </div>
                    </div>
                    <div className="content-row">
                        <div className="ls-card time-select">
                            {/* <img src={report_icon} className="ls-icon" alt="time img" /> */}
                            <MdLocationCity className="ls-icon" />
                            <p className="ls-title">Khu vực hoạt động </p>
                        </div>
                        <div className="subject_name">
                            <select
                                id="select"
                                name="files"
                                className="input_subject"
                                onChange={onChangeKV}
                            >
                                <option value="all" className="input_subject">Chọn khu vực hoạt động</option>
                                {truckInfo.area === 'Thanh Xuân' ?
                                    <option value="Thanh Xuân" className="input_subject" selected>Thanh Xuân</option> :
                                    <option value="Thanh Xuân" className="input_subject">Thanh Xuân</option>
                                }
                                {truckInfo.area === 'Hà Đông' ?
                                    <option value="Hà Đông" className="input_subject" selected>Hà Đông</option> :
                                    <option value="Hà Đông" className="input_subject">Hà Đông</option>

                                }
                                {truckInfo.area === 'Cầu Giấy' ?
                                    <option value="Cầu Giấy" className="input_subject" selected>Cầu Giấy</option> :
                                    <option value="Cầu Giấy" className="input_subject">Cầu Giấy</option>

                                }
                                {truckInfo.area === 'Hoàn Kiếm' ?
                                    <option value="Hoàn Kiếm" className="input_subject" selected>Hoàn Kiếm</option> :
                                    <option value="Hoàn Kiếm" className="input_subject">Hoàn Kiếm</option>

                                }
                                {truckInfo.area === 'Đống Đa' ?
                                    <option value="Đống Đa" className="input_subject" selected>Đống Đa</option> :
                                    <option value="Đống Đa" className="input_subject">Đống Đa</option>

                                }
                            </select>
                        </div>
                    </div>
                    <div className="content-row">
                        <div className="ls-card time-select">
                            {/* <img src={report_icon} className="ls-icon" alt="time img" /> */}
                            <FaDumpster className="ls-icon" />
                            <p className="ls-title">Điểm tập kết </p>
                        </div>
                        <div className="subject_name">
                            <select
                                id="select"
                                name="files"
                                className="input_subject"
                                onChange={onChangeDtk}
                            >
                                <option value="all" className="input_subject">Chọn điểm tập kết</option>

                                {truckInfo.gathering_address === 'Thanh Xuân' ?
                                    <option value="Thanh Xuân" className="input_subject" selected>Thanh Xuân</option> :
                                    <option value="Thanh Xuân" className="input_subject">Thanh Xuân</option>
                                }
                                {truckInfo.gathering_address === 'Hà Đông' ?
                                    <option value="Hà Đông" className="input_subject" selected>Hà Đông</option> :
                                    <option value="Hà Đông" className="input_subject">Hà Đông</option>

                                }
                                {truckInfo.gathering_address === 'Cầu Giấy' ?
                                    <option value="Cầu Giấy" className="input_subject" selected>Cầu Giấy</option> :
                                    <option value="Cầu Giấy" className="input_subject">Cầu Giấy</option>

                                }
                                {truckInfo.gathering_address === 'Hoàn Kiếm' ?
                                    <option value="Hoàn Kiếm" className="input_subject" selected>Hoàn Kiếm</option> :
                                    <option value="Hoàn Kiếm" className="input_subject">Hoàn Kiếm</option>

                                }
                                {truckInfo.gathering_address === 'Đống Đa' ?
                                    <option value="Đống Đa" className="input_subject" selected>Đống Đa</option> :
                                    <option value="Đống Đa" className="input_subject">Đống Đa</option>

                                }
                            </select>
                        </div>
                    </div>
                    <div className="content-row">
                        <div className="ls-card time-select">
                            {/* <img src={report_icon} className="ls-icon" alt="time img" /> */}
                            <FaDumpsterFire className="ls-icon" />
                            <p className="ls-title">Điểm đổ rác </p>
                        </div>
                        <div className="subject_name">
                            <select
                                id="select"
                                name="files"
                                className="input_subject"
                                onChange={onChangeDdr}
                            >
                                <option value="all" className="input_subject">Chọn điểm đổ rác</option>

                                {truckInfo.dump_address === 'Thanh Xuân' ?
                                    <option value="Thanh Xuân" className="input_subject" selected>Thanh Xuân</option> :
                                    <option value="Thanh Xuân" className="input_subject">Thanh Xuân</option>
                                }
                                {truckInfo.dump_address === 'Hà Đông' ?
                                    <option value="Hà Đông" className="input_subject" selected>Hà Đông</option> :
                                    <option value="Hà Đông" className="input_subject">Hà Đông</option>

                                }
                                {truckInfo.dump_address === 'Cầu Giấy' ?
                                    <option value="Cầu Giấy" className="input_subject" selected>Cầu Giấy</option> :
                                    <option value="Cầu Giấy" className="input_subject">Cầu Giấy</option>

                                }
                                {truckInfo.dump_address === 'Hoàn Kiếm' ?
                                    <option value="Hoàn Kiếm" className="input_subject" selected>Hoàn Kiếm</option> :
                                    <option value="Hoàn Kiếm" className="input_subject">Hoàn Kiếm</option>

                                }
                                {truckInfo.dump_address === 'Đống Đa' ?
                                    <option value="Đống Đa" className="input_subject" selected>Đống Đa</option> :
                                    <option value="Đống Đa" className="input_subject">Đống Đa</option>

                                }
                            </select>
                        </div>
                    </div>
                    <div className="btn-group_cr">
                        <Button className="btn-cancel btn_cancel"
                            onClick={(e) => handleCancel(e)}
                        >Huỷ bỏ</Button>
                        <Button className="btn-create btn_cr"
                            onClick={(e) => handleSave(e)}
                        >Lưu</Button>
                    </div>
                </div>
            </div>
            <div className="space"></div>
        </div>
    return (
        <div className="App">
            <Header />
            <div className='wrapper'>
                <LeftSide />
                <div className='main'>
                    <div className='container_center'>
                        {body}
                    </div>
                </div>
            </div>
        </div>

    )
}