import Home from './pages/home/Home'
import TruckCreation from './pages/creation/TruckCreation'
import TruckModification from './pages/modification/TruckModification'
import Notification from './pages/notification/Notification'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import TruckDetail from './pages/truck_detail/TruckDetail'
import SearchResult from './pages/search/SearchResult.js'
import Instruction from './pages/instruction/Instruction'
import './App.css';
import "./assets/scss/style.scss";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/creation" element={<TruckCreation />} />
        <Route path="/edit_truck/:truck_id" element={<TruckModification />} />
        <Route path="/notification" element={<Notification />} />
        <Route path="/truck/:id" element={<TruckDetail />} />
        <Route path='/search/:params' element={<SearchResult />} />
        <Route path="/instruction" element={<Instruction />} />
      </Routes>
    </BrowserRouter>
  );

}

export default App;
