import no_room from '../../../assets/no_room.png'
import CardTrucK from '../card_truck/CardTruck'
import { useEffect, useState } from 'react'
import PopupConfirm from '../../../popup/popup_confirm'

// import './list_truck.css'
import axios from 'axios'
// import axios from 'axios'
// import { response } from 'express'

export default function ListTruck() {
    const url_get_trucks = 'http://localhost:5000/api/trucks'
    const [listTruck, setListTruck] = useState([])
    const [listDisplay, setListDisplay] = useState([])
    const [area, setArea] = useState('Tất cả')
    const [show, setShow] = useState(false)
    const [msg, setMsg] = useState("")
    const [btnText, setBtnText] = useState("")
    useEffect(() => {
        fetchTrucks(url_get_trucks);
    }, [])
    const fetchTrucks = async (url) => {
        await axios.get(url)
            .then((res) => {
                let data = res.data
                setListTruck(data)
                setListDisplay(data)
                console.log(data)
            }).catch(err => console.log('err: ', err))
    }
    const handleDeleteTruck = (truck_id) => {
        let url_del_truck = `http://localhost:5000/api/trucks/${truck_id}`
        axios.delete(url_del_truck).then(response => {
            let status = response.status
            if (status == 200) {
                let newList = listTruck.filter(tr => {
                    return tr._id !== truck_id;
                })
                setListTruck(newList);

                let newDisplay = listDisplay.filter(tr => {
                    return tr._id !== truck_id;
                })
                setListDisplay(newDisplay)
                setMsg("Xoá xe thành công!");
            } else {
                setMsg("Xảy ra lỗi! Vui lòng kiểm tra lại");
            }
            setBtnText('Xác nhận')
            setShow(true);
        }).catch(err => {
            console.log(err)
            setMsg("Xảy ra lỗi! Vui lòng kiểm tra lại");
            setBtnText('Xác nhận')
            setShow(true);
        })

    }
    const ListTruckLine = () => {
        return listDisplay.map((truck) => {
            return (
                <CardTrucK truck={truck} onChange={handleDeleteTruck} />
            )
        })
    }
    const handleChangeArea = (e) => {
        let _area = e.target.value
        // doChangeArea(_area)
        switch (_area) {
            case 'thanh_xuan':
                setArea('Thanh Xuân')
                break;
            case 'ha_dong':
                setArea('Hà Đông')
                break;
            case 'cau_giay':
                setArea('Cầu Giấy')
                break;
            case 'hoan_kiem':
                setArea('Hoàn Kiếm')
                break;
            case 'dong_da':
                setArea('Đống Đa')
                break;
            default: setArea('Tất Cả')
        }
        if (_area === 'all') {
            setListDisplay(listTruck)
        } else {
            let list_Display = listTruck.filter(truck => {
                return truck.area == _area
            })
            setListDisplay(list_Display)
        }

    }
    return (
        <div className='container_center'>
            {show &&
                <PopupConfirm
                    title="Xoá xe"
                    handleConfirm={() => setShow(false)}
                    textBtn={btnText}
                    message={msg}
                />}
            <div className='regions'>
                <h2>Danh sách xe: {area}</h2>
                <select className='select_region' onChange={handleChangeArea}>
                    <option value='all'>Tất cả</option>
                    <option value='thanh_xuan'>Thanh Xuân</option>
                    <option value='ha_dong'>Hà Đông</option>
                    <option value='cau_giay'>Cầu Giấy</option>
                    <option value='hoan_kiem'>Hoàn Kiếm</option>
                    <option value='dong_da'>Đống Đa</option>

                </select>
            </div>
            <div className='list_truck'>
                {
                    listDisplay.length == 0 ?
                        <div className='no_truck'>
                            <img src={no_room} />
                            <h3>Chưa có xe nào^^</h3>
                        </div>

                        :
                        <div className='list_card'>
                            <ListTruckLine />
                            {/* <ListTruckLine /> */}
                        </div>
                }

            </div>
        </div>
    )
}