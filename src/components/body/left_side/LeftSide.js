import { NavLink } from 'react-router-dom'
import { notifyService } from '../../../services/notify'
import { onHasNoti1 } from '../../../services/Socket'
import React from 'react'

function LeftSide(props) {
    const currentURL = window.location.href;

    const [numOfNew, setNumOfNew] = React.useState(0);
    const [isConnectedSocket, setIsConnectedSocket] = React.useState(false);

    const fetchNON = async () => {
        const { data } = await notifyService.getNew();
        // console.log(data)
        setNumOfNew(data.new);
    }
    React.useEffect(() => {
        if (!isConnectedSocket) {
            fetchNON();
            setIsConnectedSocket(true);
            onHasNoti1(fetchNON);
        }
    }, [numOfNew])

    return (
        <div className="left_side">
            <NavLink to={`/`} className={`ls-card ${(!currentURL.includes('creation') && !currentURL.includes('notification') && !currentURL.includes('instruction')) ? 'active' : 'unactive'}`}>
                <i class="fa-solid fa-house"></i>
                <p className="ls-title">Trang chủ</p>
            </NavLink>
            <NavLink to={`/creation`} className={`ls-card ${currentURL.includes('creation') ? 'active' : 'unactive'}`}>
                {/* <img src={add} className="ls-icon" alt="img" /> */}
                <i class="fa-solid fa-truck-medical"></i>
                <p className="ls-title">Thêm xe</p>
            </NavLink>
            <NavLink to={`/notification`} className={`ls-card ${currentURL.includes('notification') ? 'active' : 'unactive'}`}>
                {/* <img src={notify} className="ls-icon" alt="img" /> */}
                <i class="fa-solid fa-bell"></i>
                <p className="ls-title">Thông báo</p>
                {numOfNew !== 0 &&
                    <p className='numOfNew' >{numOfNew}</p>}
            </NavLink>
            {/* } */}
            <NavLink to={`/instruction`} className={`ls-card ${currentURL.includes('instruction') ? 'active' : 'unactive'}`}>
                {/* <img src={tutorial} className="ls-icon" alt="img" /> */}
                <i class="fa-solid fa-book"></i>
                <p className="ls-title">Hướng dẫn</p>
            </NavLink>



        </div>
    )
}
export default LeftSide