import add from '../../../assets/add.png'
import { MdDelete, MdEdit } from 'react-icons/md'
import PopupConfirm from '../../../popup/popup_confirm'
import { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
// import './card_truck.css'

export default function CardTrucK(props) {
    const truck = props.truck
    const [deleteState, setDeleteState] = useState(false);
    const navigate = useNavigate();
    const onClickDelete = (e) => {
        setDeleteState(true)
    }

    const handleDeleteRoom = () => {
        props.onChange(truck._id)
        setDeleteState(false)
    }
    const handleClose = () => {
        setDeleteState(false)
    }
    const DialogDelete = () => {
        return (
            <PopupConfirm
                title="Xoá xe"
                handleConfirm={() => handleDeleteRoom()}
                handleCanel={() => handleClose()}
                message="Bạn muốn xoá xe này?"
            />
        )
    }
    const ViewButton = () => {
        return (
            <Link to={`/truck/${truck._id}`} >
                <div className="view-area">
                    <p className={`btn-view `}>Xem chi tiết</p>
                </div>
            </Link>
        );
    }
    return (
        // <div className='col l-3'>

            <div className="main_card" >
                {deleteState && <DialogDelete />}
                <div className="mc_header">
                    <div className="ch-text" onClick={() => navigate(`/truck/${truck._id}`)}>
                        <p className="bsx">Biển số xe: {truck.license_plate}</p>
                    </div>
                    <div className="ch-icon">
                        <Link to={`/edit_truck/${truck._id}`}>
                            <MdEdit className='icon' title='Sửa thông tin xe' />
                        </Link>
                        <MdDelete className='icon' title='Xoá xe' onClick={onClickDelete} />
                    </div>
                </div>
                <div className="mc_content">
                    <div className="card-row">
                        {/* <div className="card-content"> */}
                        <p className="lb-name">KV hoạt động:</p>
                        <p className="ls-name">{truck.area}</p>
                        {/* </div> */}
                    </div>
                    <div className="card-row">
                        {/* <div className="card-content"> */}
                        <p className="lb-name">Điểm tập kết:</p>
                        <p className="ls-name">{truck.gathering_address}</p>
                        {/* </div> */}
                    </div>
                    <div className="card-row">
                        {/* <div className="card-content"> */}
                        <p className="lb-name">Điểm đổ rác: </p>
                        <p className="ls-name">{truck.dump_address}</p>
                        {/* </div> */}
                    </div>
                    <ViewButton />
                </div>

            </div>
        // </div>

    )
}