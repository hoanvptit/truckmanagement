import React from "react";
import {
  withGoogleMap,
  withScriptjs,
  GoogleMap,
  Polygon,
  Polyline,
  Marker,
  // Polyline 
} from "react-google-maps";
import InfoBox from 'react-google-maps/lib/components/addons/InfoBox'


export default function Map({center, height, listPoint, isRoute, zoomIndex}){
  
  const Map = () => {
    const optionsPolyline = {
        strokeColor: 'red',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#ffffff',
        fillOpacity: 0.15,
        clickable: false,
        draggable: false,
        editable: false,
        visible: true,
        radius: 30000,
        zIndex: 3
      };

    const options = { closeBoxURL: '', enableEventPropagation: true };
      
    return (
      <GoogleMap defaultZoom={zoomIndex || 14} center={center} >

            {
                (listPoint.length!==0) && listPoint.map((val, index) => {
                    // console.log(val.address, val['address'])
                    return(
                        <Marker
                            position={new window.google.maps.LatLng({lat: Number(val['lat']), lng: Number(val['long'])||Number(val['lng'])})} 
                        >
                                
                            <InfoBox options={options} >
                            <>
                                <div style={{ backgroundColor: 'rgb(54, 88, 199)', color: 'white', borderRadius:'10px', padding: '3px' }}>
                                {index+1}
                                </div>
                            </>
                            </InfoBox>  
                                    
                        </Marker>
                    )
                })
            }
            <Polyline path={listPoint} options={optionsPolyline} />
        </GoogleMap>
    );
  }
  const MapComponent = withScriptjs(withGoogleMap(Map));
  return <MapComponent
    googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
    loadingElement={<div style={{ height: `500px`, backgroundColor: '#ccc' }} />}
    containerElement={<div style={{ height: `${height? height: '500px'}`, width: "100%", borderRadius: '10px'}} />}
    mapElement={<div style={{ height: `500px`, borderRadius: '5px' }} />}
  />
}
