import React, { useState } from 'react';
import { MdSearch } from 'react-icons/md';
import { Link, NavLink } from 'react-router-dom';
// import './header.css'
function Header() {
    const [searchInput, setSearchInput] = useState('')
    const ref = React.createRef()
    const handleSearch = () => {
        if (searchInput.trim() === '') {
            setSearchInput('');
            alert('Vui lòng nhập thông tin tìm kiếm')
        }
        else {
            ref.current.click()
        }

    }

    const handleSearch2 = (e) => {
        let keycode = e.keyCode;
        if (keycode === 13) {
            handleSearch();
        }
    }
    
    return (
        <div className="header">
            <div className='left_menu'>
                <label htmlFor='nav_mobile_input'>
                    <img className="menu_icon"
                        src="https://img.icons8.com/ios-glyphs/30/000000/menu--v1.png" />
                </label>

                <Link to={`/`} className='link_title'>
                    <h2 className='title'>
                        Quản lý xe đô thị
                    </h2>
                </Link>
            </div>
            <div className="search">
                {searchInput.trim() !== '' ? <Link to={`/search/${searchInput}`} title='Tìm kiếm theo biển số xe' ref={ref}>
                    <MdSearch
                        className="search-btn"
                    />
                </Link> :
                    <MdSearch
                        className="search-btn"
                        onClick={(e) => handleSearch(e)}
                    />
                }
                <input
                    className="input-txt"
                    type="text" placeholder="Nhập thông tin xe cần tìm "
                    value={searchInput}
                    onChange={(e) => { setSearchInput(e.target.value) }}
                    onKeyDown={(e) => handleSearch2(e)}
                />
            </div>
            <input id='nav_mobile_input' className='nav_input' type='checkbox' />
            <label htmlFor='nav_mobile_input' className='nav_overlay'>

            </label>
            <div className='nav_mobile'>
                <label htmlFor='nav_mobile_input' className='nav_mobile_close'>
                    <i class='fa fa-times'></i>
                </label>
                <div className='nav_list'>
                    <NavLink to={`/`} className={`ls-card`}>
                        <i class="fa-solid fa-house"></i>
                        <p className="ls-title">Trang chủ</p>
                    </NavLink>
                    <NavLink to={`/creation`} className={`ls-card`}>
                        <i class="fa-solid fa-truck-medical"></i>
                        <p className="ls-title">Thêm xe</p>
                    </NavLink>
                    <NavLink to={`/notification`} className={`ls-card`}>
                        <i class="fa-solid fa-bell"></i>
                        <p className="ls-title">Thông báo</p>

                    </NavLink>
                    <NavLink to={`/instruction`} className={`ls-card`}>
                        <i class="fa-solid fa-book"></i>
                        <p className="ls-title">Hướng dẫn</p>
                    </NavLink>
                </div>

            </div>
        </div>
    )
}
export default Header