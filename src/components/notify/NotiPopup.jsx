import React, { Component } from "react"

export default function NotiPopup({noti, hide}) {
    return (
        <div className="popup-background">
            <div className="popup-noti-wrapper">
                <div className="popup-noti-title">{noti.title}</div>
                <div className="popup-noti-content">{noti.content}</div>
                <img src={process.env.REACT_APP_BASE_URL+'/'+noti.image} />
                <div className="popup-noti-footer">
                    <button className="popup-btn-confirm"  
                        onClick={ ()=>{ hide() } }>OK</button>
                </div>
            </div>
        </div>
    )
}

