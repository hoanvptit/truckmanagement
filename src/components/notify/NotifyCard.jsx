import React from 'react'
import moment from 'moment';
import {notifyService} from '../../services/notify'
import NotiPopup from './NotiPopup'

function NotifyCard(props) {
    const noti = props.noti;
    // const [showNoti, setShowNoti] = React.useState(false);

    return (
        <div className="noti_card"
            onClick={() => {
                notifyService.read(noti._id);
                props.handleClick()
                // setShowNoti(pre => !pre)
            }}
        >
            {/* {showNoti && <NotiPopup noti={noti} hide={() => setShowNoti(false)}/>} */}
            <div className="ntcard_left" >
                <i class="fa-solid fa-bell"></i>
            </div>
            <div className="ntcard_right">
                <div className='title_div'>
                    <h3>{noti.title}</h3>
                    {
                        noti.isReaded
                        ?<h5>Đã xem</h5>
                        :<h5 style={{color: "red"}}>Chưa xem</h5>
                    }
                </div>
                <p>{moment(noti.time).format('hh:mm:ss   DD/MM/yyyy')}</p>
                <h4>{noti.content}</h4>
            </div>
        </div>
    )
}

export default NotifyCard