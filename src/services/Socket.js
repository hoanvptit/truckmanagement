import io from 'socket.io-client'

const socket = io('ws://localhost:5001');

export const onHasNoti1 = (fetchNON) => {
    socket.on('hasNewNoti', (data) => {
        fetchNON();
    })
}

export const onImage = (lisence_plate, changeImage) => {
    socket.on('stream'+lisence_plate, (time, data) => {
        changeImage(time, data)
    })
}
