import axios from "axios";


export const routeService = {
  async getRouteOfTruck(truckId, date) {
    console.log(truckId)
    return await axios.get(`${process.env.REACT_APP_BASE_URL}/api/routes/${truckId}/${date}`);
  },
  
};
