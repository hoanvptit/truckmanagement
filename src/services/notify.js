import axios from "axios";


export const notifyService = {
  async getNotify() {
    return await axios.get(`${process.env.REACT_APP_BASE_URL}/api/notify`);
  },
  async getNew() {
    return await axios.get(`${process.env.REACT_APP_BASE_URL}/api/notify/new`);
  },
  async read(_id) {
    return await axios.post(`${process.env.REACT_APP_BASE_URL}/api/notify`, {id: _id});
  },
  
};
