import axios from "axios";


export const truckService = {
  async getById(truckId) {
    return await axios.get(`${process.env.REACT_APP_BASE_URL}/api/trucks/${truckId}`);
  },
  
};
