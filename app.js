const express = require('express')
const app = express()

const listTruck = [
    {
        id_gis: '1',
        license_plate:'1234',
        area:'Thanh xuan',
        dump_address:'Nguyen Trai',
        gathering_address:'Nguyen Trai'
    },
    {
        id_gis: '2',
        license_plate:'12cd',
        area:'Thanh xuan',
        dump_address:'Nguyen Trai',
        gathering_address:'Nguyen Trai'
    },
    {
        id_gis: '3',
        license_plate:'1234',
        area:'Thanh xuan',
        dump_address:'Nguyen Trai',
        gathering_address:'Nguyen Trai'
    },
    {
        id_gis: '4',
        license_plate:'12cd',
        area:'Thanh xuan',
        dump_address:'Nguyen Trai',
        gathering_address:'Nguyen Trai'
    },
    {
        id_gis: '5',
        license_plate:'1234',
        area:'Thanh xuan',
        dump_address:'Nguyen Trai',
        gathering_address:'Nguyen Trai'
    },
    {
        id_gis: '6',
        license_plate:'12cd',
        area:'Thanh xuan',
        dump_address:'Nguyen Trai',
        gathering_address:'Nguyen Trai'
    },
    {
        id_gis: '7',
        license_plate:'1234',
        area:'Thanh xuan',
        dump_address:'Nguyen Trai',
        gathering_address:'Nguyen Trai'
    },
    {
        id_gis: '8',
        license_plate:'12cd',
        area:'Thanh xuan',
        dump_address:'Nguyen Trai',
        gathering_address:'Nguyen Trai'
    }
]
app.get('/api/trucks', (req, res)=>res.json(listTruck))
app.listen(4000, ()=>console.log('Anh Hoa dep trai'))